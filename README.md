# Kult Byrå Developer Task

This assignment has been completed using Vue.js and the Sinatra web framework. To run locally you must have Ruby and NPM installed on your machine. Otherwise, to see the app in action visit [https://kultdesign.herokuapp.com](https://kultdesign.herokuapp.com).  The app is running on a free server that sleeps, so the first time you visit the link it will take some time to wake the dyno.

## Running Locally

1. **Install ruby dependencies**

    If you do not have the bundler gem installed, first install it.

    `$ gem install bundler`

    Then, install the projects ruby dependencies:

    `$ bundle install`

1. **Install node modules**

    `$ npm install`

1. **Build source code**

    After you have everything installed, you need to build the raw source.

    `$ npm run build`

1. **Start Sinatra server**

    Now, the app is ready to be served. Run the following in the projects root directory:

    `$ ruby server.rb`

    The app will now be running locally on port 9292, unless you have a $PORT environment variable set on your machine.

## Features
- Responsive grid and input fields 
- Basic field validation and error handling
- Submitted contacts stored in memory
- Modular component design for easy extendability
- References to the greatest cartoon ever made

## Possible Improvements

- Server side validation only checks that all fields have content and does not validate the inputs, for example that an email is properly formatted.
- Client side validation provides faster feedback and would reduce strain on the server.
- Presently data is only stored in memory, meaning that each time the server sleeps or is restarted everything is lost (except the dummy data). In a real scenario, a database such as Postgresql would be used, which would also make server side validation cleaner and easier.

© Erik Griffin 2018
