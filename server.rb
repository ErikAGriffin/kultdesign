require 'sinatra/base'
require 'oj'

# :compat allows symbol to string JSON dump
Oj.default_options = {:mode=>:compat,:symbol_keys=>true}

class Server < Sinatra::Base
  set :public_folder, File.dirname(__FILE__) + '/dist'
  set :port, ENV['PORT'] || 9292

  configure :production, :development do
    enable :logging
  end

  def initialize
    @@contact_index = 0
    @@contact_cards = []
    init_cards
    super
  end

  get '/' do
    render_static 'index.html'
  end

  post '/new_contact' do
    contact = Oj.load(request.body.read)
    if is_valid? contact
      add_to_contacts contact
      200
    else
      400
    end
  end

  get '/contact_cards' do
    Oj.dump(@@contact_cards)
  end

  get '*' do
    render_static 'index.html'
  end

  def render_static(file_name)
    send_file File.join(settings.public_folder, file_name)
  end

  def is_valid?(contact)
    if !contact[:name] || contact[:name].empty?
      false
    elsif !contact[:email] || contact[:email].empty?
      false
    elsif !contact[:address]
      false
    elsif !contact[:address][:street] || contact[:address][:street].empty?
      false
    elsif !contact[:address][:city] || contact[:address][:city].empty?
      false
    elsif !contact[:address][:zipcode] || contact[:address][:zipcode].empty?
      false
    else
      true
    end
  end

  def init_cards
    smith_home = {
      :street=>"155 Laurel Lane",
      :city=>"Santa Fé",
      :zipcode=>"87505"
    }
    rick = {
      :name=>"Rick Sanchez",
      :email=>"pickle@rick.awesome",
      :address=> smith_home
    }
    morty = {
      :name=>"Morty Smith",
      :email=>"niceguy06@mail.com",
      :address=> smith_home
    }
    summer = {
      :name=>"Summer Smith",
      :email=>"butterflies2001@mail.com",
      :address=> smith_home
    }
    add_to_contacts summer
    add_to_contacts morty
    add_to_contacts rick
  end

  def add_to_contacts(contact)
    contact[:id] = @@contact_index
    @@contact_index += 1
    @@contact_cards.unshift contact
  end

  run! if app_file == $0
end
